function [keypoints, countmax] = Sift2D_ajay(inputImage, sigmaScale, octaveScale)



inputImage = (inputImage - min(min(min(inputImage))) )./(max(max(max(inputImage))) - min(min(min(inputImage))));

%inputImage = inputImage./255;
%inputImage = inputImage - (min(inputImage(:)));
contrastThresh = 0.05;

sigma = 0.707107;

sigma = sigmaScale*sigma;
sizeinput = size(inputImage);

%% for each octave

%octaves = [2, 1, 0.5, .25];

octaves = octaveScale*[1, 0.5, 0.25, 0.125];

countmax = 1;

countmin = 1;

for j=1:4
    
    inputImageOctave = image_resize(inputImage, sizeinput(1)*octaves(j), sizeinput(2)*octaves(j));
    
    count = 1;
    
    
    
    for i=1:5
        
        imageScaled(:,:,i) = imgaussian(inputImageOctave, sigma*i);
        
        if(i>1)
            
            %eval(sprintf('DoG_octave%d(:,:,:,count) = imageScaled(:,:,:,i-1) - imageScaled(:,:,:,i);',j));
            DoG_octave(:,:,count) = imageScaled(:,:,i-1) - imageScaled(:,:,i);
            
            count = count+1;
            
        end
        
    end
    
    clear imageScaled;
    
    %end
    
    
    
    %% get local maxima/minima
    
    %eval(sprintf('[a,b,c,d] = size(DoG_octave%d(:,:,:,i))', j));
    
    
    
    
    for i=2:3
        
        % maxima(:,:,:,i) = localMaxima(squeeze(DoG_octave(:,:,:,i)), 1);
        
        
        [a,b] = size(DoG_octave(:,:,i));
        
        for u=2:a-1
            for v=2:b-1
                
                
                
                %eval(sprintf('cont = DoG_octave%d(u,v,w,i)', j));
                
                cont = DoG_octave(u,v,i);
                
                if(abs(cont)>contrastThresh)
                    
                    %% check if local maxima
                    
                    %eval(sprintf('subvol = squeeze(DoG_octave%d(u-1:u+1, v-1:v+1, w-1:1+1, i))', j));
                    subvol = (DoG_octave(u-1:u+1, v-1:v+1, i));
                    
                    if (cont==max(max(max(subvol))))
                        
                        %%check other scales
                        subvol = (DoG_octave(u-1:u+1, v-1:v+1, i-1));
                        
                        if(cont>=max(max(max(max(subvol)))))
                            
                            subvol = (DoG_octave(u-1:u+1, v-1:v+1, i+1));
                            
                            if(cont>=max(subvol(:)))
                                
                                
                                localMaxima(countmax,1) = u/octaves(j);
                                localMaxima(countmax,2) = v/octaves(j);
                                
                                
                                
                                countmax =countmax+1;
                                
                            end
                        end
                    end
                    
                    %%% check if local minima
                    
                    %          if (cont==min(min(min(subvol))))
                    
                    %            %%check other scales
                    %            subvol = (DoG_octave(u-1:u+1, v-1:v+1, w-1:w+1, i-1));
                    %
                    %            if(cont<=min(min(min(min(subvol)))))
                    % %
                    %                 subvol = (DoG_octave(u-1:u+1, v-1:v+1, w-1:w+1, i+1));
                    %
                    %                  if(cont<=min(min(min(min(subvol)))))
                    %
                    %
                    %                      localMinima(countmin,1) = u/octaves(j);
                    %                      localMinima(countmin,2) = v/octaves(j);
                    %                      localMinima(countmin,3) = w/octaves(j);
                    %
                    %                      countmin = countmin+1
                    %
                    %                  end
                    %             end
                    %          end
                    
                    
                end
                
                
                
                %%end for loops
                
            end
        end
        
    end
    
    
    clear DoG_octave;
    clear subvol;
    clear imageScaled;
    
end



%
% for i=1:size(localMinima)
%
%     inputImage(round(localMinima(i,1)), round(localMinima(i,2)), round(localMinima(i,3))) = 1;
%
%
% end



countmax = countmax-1;
if(exist('localMaxima'))
    keypoints  = localMaxima;
else
    keypoints = -1;
end











