[OutfilesRaw] = rdir('Y:\CAD0examples\RawScanData\*\**\*\*.img');
[OutfilesHypo] = rdir('Y:\CAD0examples\CNNsegmentationData_20160614\*\**\*\*_HypoScattering.tif');

for caseId = 1:23
    
    matfiles = 
%s = what; %look in current directory
s=what('Y:\tmpMount\CADeGroundTruth\HypoScatteringGroundTruth\Patient16\RegionLabels_TifStack_16-03-07_10-45-43_F6_9530120_121220130018_Polar') %change dir for your directory name ;

FilePath = '\\IMGPROCRAID\DataRAID\AMCdata\Gen2L_FIH\PatientData\GEN2LFIM_16\16-03-07_10-34-22_9530120\F6\16-03-07_10-45-43_F6_9530120_121220130018.img';

objScanData = ScanDataClass_WholeScan(FilePath);

LoadNPMMFile(objScanData);

BalloonLoc = objScanData.BalloonLocation;


matfiles=s.mat;
cd(s.path);
for frNum=1:numel(matfiles)
    disp(frNum)
 %frNum = 216;
load(char(matfiles(frNum)));

imageLab = zeros(ImgParams.nSamplesPerAline, ImgParams.nAlinesPerFrame);
imageLab(sub2ind(size(imageLab), Labels.Depth(:), Labels.Aline(:))) = 1;

unFlatImageLab = UnFlattenImage(imageLab, round(objScanData.BalloonLocation(1:2:end,frNum)/2) );

scanConv = ScanConvert(unFlatImageLab, 1000, 0.25, 1);

[stats, statImage] = GetStatInfo(scanConv);

statsAllFrames(frNum).stats = stats;

%[stats, statImage] = GetStatInfo(unFlatImageLab);
% PolarStatImage(frNum).Solidity  = ImToPolar(statImage.Solidity, 0, 1, 512, 1024);
% PolarStatImage(frNum).Eccentricity = ImToPolar(statImage.Eccentricity, 0, 1, 512, 1024);
% PolarStatImage(frNum).NonConvexArea = ImToPolar(statImage.NonConvexArea, 0, 1, 512, 1024);
% 
% enFaceImage.Solidity(frNum,:) = sum(PolarStatImage(frNum).Solidity, 1);
% enFaceImage.Eccentricity(frNum,:) = sum(PolarStatImage(frNum).Eccentricity, 1);
% enFaceImage.NonConvexArea(frNum,:) = sum(PolarStatImage(frNum).NonConvexArea, 1);
end
