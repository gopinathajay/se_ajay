
clear

%FilePath  = '\\imgprocRAID\DataRAID\AMCdata\Gen2L_FIH\PatientData\GEN2LFIM_12\16-02-22_11-01-01_9530120\F3\16-02-22_11-16-10_F3_9530120_121220130018.img';
%FilePath = '\\imgprocRAID\DataRAID\RegistryData\06-Temple\06_056_01\15-01-23_13-40-06_9510120\F6\15-01-23_13-45-50_F6_9510120_040720140025.img';

%FilePath = '\\IMGPROCRAID\DataRAID\AMCdata\Gen2L_FIH\PatientData\GEN2LFIM_09\16-02-19_12-47-51_9530120\F3\16-02-19_12-55-39_F3_9530120_121220130018.img';

%FilePath = '\\IMGPROCRAID\DataRAID\AMCdata\Gen2L_FIH\PatientData\GEN2LFIM_16\16-03-07_10-34-22_9530120\F6\16-03-07_10-45-43_F6_9530120_121220130018.img';
%FilePath = '\\IMGPROCRAID\DataRAID\AMCdata\Gen2L_FIH\PatientData\GEN2LFIM_16\16-03-07_10-34-22_9530120\S5\16-03-07_10-44-49_S5_9530120_121220130018.img';
%FilePath = '\\IMGPROCRAID\DataRAID\AMCdata\Gen2L_FIH\PatientData\GEN2LFIM_17\16-03-07_14-13-55_9530120\F3\16-03-07_14-20-19_F3_9530120_121220130018.img';

%FilePath = '\\IMGPROCRAID\DataRAID\AMCdata\Gen2L_FIH\PatientData\GEN2LFIM_17\16-03-07_14-13-55_9530120\S2\16-03-07_14-19-37_S2_9530120_121220130018.img';
FilePath = '\\IMGPROCRAID\DataRAID\AMCdata\Gen2L_FIH\PatientData\GEN2LFIM_15\16-03-04_12-27-20_9530120\F7\16-03-04_12-38-51_F7_9530120_121220130018.img';

%FilePath = '\\IMGPROCRAID\DataRAID\AMCdata\Gen2L_FIH\PatientData\GEN2LFIM_15\16-03-04_12-27-20_9530120\S5\16-03-04_12-36-15_S5_9530120_121220130018.img';
%first hand scan from dane
%FilePath  = 'C:\Data\MICRO_10\16-03-03_14-27-34_BalloonSize20MM\F1\16-03-03_14-27-48_F1_BalloonSize20MM_032520130001.img';

%Scattering phantom
%FilePath = 'C:\Data\MICROPEN18_\16-03-10_15-35-23_BalloonSize20MM\F2\16-03-10_15-40-32_F2_BalloonSize20MM_032520130001.img';

%phantom layered
%FilePath = 'C:\Data\MICROPEN18_\16-03-10_15-35-23_BalloonSize20MM\F1\16-03-10_15-37-33_F1_BalloonSize20MM_032520130001.img';

%hand scan
%FilePath = 'T:\tmpMount\MICROPEN18\16-03-10_14-59-28_BalloonSize20MM\F3\16-03-10_15-07-32_F3_BalloonSize20MM_032520130001.img';


%75% loading
%FilePath = 'C:\Data\MICRO_07\16-03-03_15-37-43_BalloonSize20MM\F1\16-03-03_15-38-03_F1_BalloonSize20MM_032520130001.img';


%50% loading
%FilePath = 'C:\Data\MICRO_04\16-03-03_14-23-23_BalloonSize20MM\F1\16-03-03_14-23-37_F1_BalloonSize20MM_032520130001.img';

%25% loading
%FilePath = 'C:\Data\MICRO_01\16-03-03_13-44-59_BalloonSize20MM\F1\16-03-03_13-46-50_F1_BalloonSize20MM_032520130001.img';

objScanData = ScanDataClass_WholeScan(FilePath);

LoadNPMMFile(objScanData);


BalloonLoc = objScanData.BalloonLocation;


BuildIntensityTemplate

padNum = 100;
numFramesProcessed = 1;


%figure; 
%for frames = [1:objScanData.nFramesInScan]
for frames = 748

    disp(frames)
    
    image_range = ReadFramesInRange(frames, objScanData);
    
    %image_range = f_ComputeAttenCoeffImage(image_range, 6);
    
    numALines = objScanData.nAlinesPerFrame;
    
    %pad by 50 A-lines on either side
    
    image_range_flat_unpadded = FlattenImage(image_range, objScanData.BalloonLocation(:,frames));
    
    GlandDetectHessianBased;
    
    
    
    image_range_flat = [image_range_flat_unpadded(:,end-padNum+1:end), image_range_flat_unpadded, image_range_flat_unpadded(:,1:padNum)];
    
    image_binMask = GenBinaryTissueMask(image_range, objScanData.BalloonLocation(:,frames));
    
    continue;
    
    image_binMask_flat_unpadded = FlattenImage(image_binMask, objScanData.BalloonLocation(:,frames));
    
    image_binMask_flat = [image_binMask_flat_unpadded(:,end-padNum+1:end), image_binMask_flat_unpadded, image_binMask_flat_unpadded(:,1:padNum)];
    
preProcSift;


label1 = bwlabel(1-image_binMask_flat(1:500,:));

image_conncomp = bwconncomp(1-image_binMask_flat(1:500,:));

areaProps = struct2array(regionprops(image_conncomp, 'Area'));

validIdx = find(areaProps > 1000 & areaProps < 20000);

label1_valid = zeros(size(label1));

label1_valid(ismember(label1, validIdx)) = 1;
[kp_r, kp_c] = find(siftPoint_resize>0);

figure; 
subplot(2,1, 1); imagesc(image_range_flat(1:500,:)); colormap(gray)
title(strcat('frame: ', int2str(frames)));
hold on; plot(kp_c, kp_r, 'r*')
subplot(2,1, 2); imagesc(label1_valid); colormap(gray)
% saveas(gcf, strcat('frame', int2str(frames), '.tif'));
% close all;

enFace_Glands_mask(:,frames) = sum(label1_valid(:,padNum+1:padNum+numALines),1);

end



return; 

%Gland Detection
[kp, cmax] = Sift2D_ajay(1-f_ComputeAttenCoeffImage(double(image_range_flat(1:500, :)), 6));
figure; imagesc(image_range_flat(1:500, :)); colormap(gray); 
hold on; plot(kp(:,2), kp(:,1), 'r*')

label1 = bwlabel(1-image_binMask_flat(1:500,:));

image_conncomp = bwconncomp(1-image_binMask_flat(1:500,:));

areaProps = struct2array(regionprops(image_conncomp, 'Area'));

validIdx = find(areaProps > 1000 & areaProps < 20000);

label1_valid = zeros(size(label1));

label1_valid(ismember(label1, validIdx)) = 1;

image_Glands_mask(:,:,frames) = label1_valid;
image_gray(:,:,frames) = image_range_flat(1:500,:);



