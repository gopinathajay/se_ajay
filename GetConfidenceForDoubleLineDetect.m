confidenceZone = 5;
trackDoubleLine = true;
searchZone = 200; % search in this zone while tracking the doubleLine
minFramesForTracking = 2;

if(frames==41)
    stophere =1;
end
%if (listofDoubleLine2(:,frames) > -1 && listofDoubleShadow(:,frames) > -1)
    
    %check if they are near by
    dbLine = listofDoubleLine2(:,frames);
    dblShd = listofDoubleShadow(:,frames);
    
    if(dbLine == -1)
        doubleLineConfidence(:,frames) = 0;
        return;
    end
    
    if( abs(dbLine - dblShd) < confidenceZone || abs( dbLine + numALines - dblShd) < confidenceZone || abs(dblShd + numALines - dbLine) < confidenceZone)
        
        doubleLineConfidence(:,frames) = 5;
        
    else
        
        doubleLineConfidence(:,frames) = 0;
        
    end
    
    if(doubleLineConfidence(:,frames) == 0)
        if(trackDoubleLine)
            
            
            if(numFramesProcessed > minFramesForTracking)
                
                
                dbLine_prev = listofDoubleLine2(:,frames-1);
                conf_prev = doubleLineConfidence(:, frames -1);
                
                %if the double line intensity peak is close to the previous frame's
                %high confidence mark, then use that
                
                if( abs(dbLine_prev - dbLine) < searchZone || ...
                        abs( dbLine + numALines - dbLine_prev) < searchZone || ...
                        abs(dbLine_prev + numALines - dbLine) < searchZone )
                    % within the search zone & previous confidence value
                    % was high. Then current confidence is set to 4 and use
                    % current peak.
                    if(conf_prev >= 3)
                        doubleLineConfidence(:,frames) = 4;
                    end
                    
                    
                    % else search for peaks in the search zone based on previous frame
                    
                elseif(dbLine_prev > -1)
                    
                    dbLine_prev = dbLine_prev + padNum;
                    
                    if( (dbLine_prev + searchZone) > length(vecDoublePeaks))
                        vecDoublePeaksTemp = [vecDoublePeaks, vecDoublePeaks];
                        [maxVal, peakDouble2] = max(vecDoublePeaksTemp((dbLine_prev - searchZone): (dbLine_prev + searchZone)));
                        
                        if(maxVal >0)
                            if(peakDouble2 > length(vecDoublePeaks))
                                peakDouble2 = length(vecDoublePeaks) - peakDouble2;
                            end
                            peakDouble2  = peakDouble2 - searchZone + dbLine_prev - padNum;
                            doubleLineConfidence(:,frames) = 3;
                            
                             listofDoubleLine2(:,frames) = peakDouble2;
                        else
                            doubleLineConfidence(:,frames) = 2; %% no detection of peaks
                            peakDouble2  = -1;
                            
                        end
                        
                    elseif( (dbLine_prev - searchZone) < 1)
                         vecDoublePeaksTemp = [vecDoublePeaks, vecDoublePeaks];
                         dbLine_prev = dbLine_prev + length(vecDoublePeaks);
                        [maxVal, peakDouble2] = max(vecDoublePeaksTemp((dbLine_prev - searchZone): (dbLine_prev + searchZone)));
                        
                         if(maxVal >0)
                            if(peakDouble2 > length(vecDoublePeaks))
                                peakDouble2 = length(vecDoublePeaks) - peakDouble2;
                            end
                            peakDouble2  = peakDouble2 - searchZone + dbLine_prev - padNum;
                            doubleLineConfidence(:,frames) = 3;
                            
                             listofDoubleLine2(:,frames) = peakDouble2;
                        else
                            doubleLineConfidence(:,frames) = 2; %% no detection of peaks
                            peakDouble2  = -1;
                            
                         end
                        
                        
                    else
                    
                    [maxVal, peakDouble2] = max(vecDoublePeaks((dbLine_prev - searchZone): (dbLine_prev + searchZone)));
                    
                    if(maxVal >0)
                        peakDouble2  = peakDouble2 - searchZone + dbLine_prev - padNum;
                        doubleLineConfidence(:,frames) = 3;
                        
                         listofDoubleLine2(:,frames) = peakDouble2;
                    else
                        doubleLineConfidence(:,frames) = 2; %% no detection of peaks
                        peakDouble2  = -1;
                        
                    end
                    
                    end
                    
                   
                    
                end
                
            end
            
            
            
            
            
        end
        
    end
    
    
%end