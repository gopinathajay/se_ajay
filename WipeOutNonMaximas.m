function vec1 = WipeOutNonMaximas(vecin)

[val, loc] = max(vecin);

vec1 = zeros(size(vecin));

range = loc-35:loc+35;
range(find(range<=0)) = range(find(range<=0)) + 4096;

range(find(range>4096)) = range(find(range>4096)) - 4096;

vec1(range) = vecin(range);

