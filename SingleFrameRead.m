close all
clearvars
clc


FilePath = 'T:\NewRegLineData\LN01350_2\14-06-02_09-33-28_BalloonSize20MM\F2\Snapshot_F965_A3938\Frame.img';

AlinesPerFrame = 4096;
SamplesPerAline = 2048;
FrameStartIndex = 1;
NumberOfFrames = 1;

fid = fopen(FilePath,'r','l');
fseek(fid, (FrameStartIndex-1)*SamplesPerAline*AlinesPerFrame, 'bof');
Data = fread(fid,[SamplesPerAline, AlinesPerFrame*NumberOfFrames],'uint8');
Data = reshape(Data, SamplesPerAline, AlinesPerFrame, NumberOfFrames);
fclose(fid);

%%

PlotIndex = 1;

Image = squeeze(Data(:, :, PlotIndex));

figure; imagesc(Image); colormap(gray)
