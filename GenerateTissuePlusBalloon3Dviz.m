
%FilePath = '\\imgprocRAID\DataRAID\AMCdata\Gen2L_FIH\PatientData\GEN2LFIM_16\16-03-07_10-34-22_9530120\F8\16-03-07_10-56-10_F8_9530120_121220130018.npmm';
%FilePath = '\\imgprocRAID\DataRAID\RegistryData\03-Mayo Clinic Jacksonville\03_031_01\15-06-16_10-13-06_9510120\F4\15-06-16_10-23-12_F4_9510120_031320140023.npmm';
FilePath = '\\imgprocRAID\DataRAID\RegistryData\10-NorthShore-LIJ\10_067_01\15-04-13_10-04-44_9510120\F3\15-04-13_10-09-32_F3_9510120_100820130011.npmm';

[Header, Data] = f_LoadNPMMFile(FilePath);

%FilePath = '\\imgprocRAID\DataRAID\AMCdata\Gen2L_FIH\PatientData\GEN2LFIM_16\16-03-07_10-34-22_9530120\F8';
%FilePath = '\\imgprocRAID\DataRAID\RegistryData\03-Mayo Clinic Jacksonville\03_031_01\15-06-16_10-13-06_9510120\F4';
FilePath = '\\imgprocRAID\DataRAID\RegistryData\10-NorthShore-LIJ\10_067_01\15-04-13_10-04-44_9510120\F3';

%objScanData = ScanDataClass_WholeScan('\\imgprocRAID\DataRAID\AMCdata\Gen2L_FIH\PatientData\GEN2LFIM_16\16-03-07_10-34-22_9530120\F8\16-03-07_10-56-10_F8_9530120_121220130018.img');
%objScanData = ScanDataClass_WholeScan('\\imgprocRAID\DataRAID\RegistryData\03-Mayo Clinic Jacksonville\03_031_01\15-06-16_10-13-06_9510120\F4\15-06-16_10-23-12_F4_9510120_031320140023.img');
objScanData = ScanDataClass_WholeScan('\\imgprocRAID\DataRAID\RegistryData\10-NorthShore-LIJ\10_067_01\15-04-13_10-04-44_9510120\F3\15-04-13_10-09-32_F3_9510120_100820130011.img');

%load('\\imgprocRAID\DataRAID\CAD0examples\CNNsegmentationData_20160614\GEN2LFIM_16\16-03-07_10-34-22_9530120\F8\TissueSurface.mat');
%load('\\imgprocRAID\DataRAID\CAFE\CAFE_ProcessedScans\DataForDDWAbstracts\03_031_01\15-06-16_10-13-06_9510120\F4\CNNsegTifStacks\TissueSurface.mat');
load('\\imgprocRAID\DataRAID\CAFE\CAFE_ProcessedScans\DataForDDWAbstracts\10_067_01\15-04-13_10-04-44_9510120\F3\CNNsegTifStacks\TissueSurface.mat');


LoadNPMMFile(objScanData);

BalloonLoc = objScanData.BalloonLocation;
for j=1:1:1200
    
    BalloonLocCorrected = circshift(BalloonLoc(:,j), Data.RegLine(j));
    BalloonLocCorrectedAll(j, :) = circshift(BalloonLoc(:,j), Data.RegLine(j));
    TissueSurface_plusBalloon(j,:) = TissueSurface(j,:)*2 + BalloonLocCorrected(1:4:end)' -10;
    TissueSurface_plusBalloonFullRes(j,:) = TissueSurfaceFullRes(j,:) + BalloonLocCorrected(1:1:end)' -10;
    
    BalloonSurfaceOnly(j,:) = BalloonLocCorrected(1:1:end)' - 10;
    
    if(~isempty(find(TissueSurface(j,:) ==0)))
        
        %TissueSurface_plusBalloon(j,find(TissueSurface(j,:) ==0)) = 1000;
        TissueSurface_plusBalloon(j,find(TissueSurface(j,:) ==0)) = 0;
        
    end
    
    for ai = 1:1024
        x(ai,j) = TissueSurface_plusBalloon(j,ai) * sin(ai*2*pi/1024);
        y(ai,j) = TissueSurface_plusBalloon(j,ai) * cos(ai*2*pi/1024);
        z(ai,j) = j;
        
    end
    
    for bi = 1:4096
        xb(bi, j) = BalloonLoc(bi, j)* sin(bi*2*pi/4096);
        yb(bi, j) = BalloonLoc(bi,j) * cos(bi*2*pi/4096);
        zb(bi, j) = j;
    end
    
end

figure
%surf(xb,yb,zb,'FaceColor','interp',...
surf(x,y,z,'FaceColor','interp',...
'EdgeColor','none',...
'FaceLighting','flat');daspect([5 5 1])
axis tight
view(-50,30)
camlight left
colormap copper
% fileID = fopen('Y:\CAD0examples\CNNsegmentationData_20160614\GEN2LFIM_16\16-03-07_10-34-22_9530120\F8\TissueSurfacePlusBalloon.bin', 'w');
% 
% fwrite(fileID,TissueSurface_plusBalloon,'uint16');

%TissueSurface_plusBalloonFullRes = circshift(TissueSurface_plusBalloonFullRes, 2048, 2);

%fileID = fopen('\\imgprocRAID\DataRAID\CAFE\CAFE_ProcessedScans\DataForDDWAbstracts\03_031_01\15-06-16_10-13-06_9510120\F4\CNNsegTifStacks\TissueSurfacePlusBalloonTransPosedFullRes.bin', 'w');
fileID = fopen('\\imgprocRAID\DataRAID\CAFE\CAFE_ProcessedScans\DataForDDWAbstracts\10_067_01\15-04-13_10-04-44_9510120\F3\CNNsegTifStacks\TissueSurfacePlusBalloonTransposedFullRes.bin', 'w');
fwrite(fileID,TissueSurface_plusBalloonFullRes','uint16');

fileID = fopen('\\imgprocRAID\DataRAID\CAFE\CAFE_ProcessedScans\DataForDDWAbstracts\10_067_01\15-04-13_10-04-44_9510120\F3\CNNsegTifStacks\BalloonSurfaceOnly.bin', 'w');
fwrite(fileID,BalloonSurfaceOnly','uint16');


