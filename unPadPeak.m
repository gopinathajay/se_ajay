function unpadP = unPadPeak(peak, numALines, padNum)

if peak > padNum & peak <= (numALines + padNum)
    unpadP = peak - padNum;
elseif peak <= padNum   % peak was detected in the padded region
    unpadP = numALines - padNum + peak; % move it to the latter part since it was padded from there
else %peak is in the last padded region, move it back to the beginning
    unpadP = peak - numALines - padNum;
end