%Hessian based approach

[Dxx,Dxy,Dyy] = Hessian2D(imcomplement(double(image_range_flat_unpadded)), 15); 

[l1, l2, ix, iy] = eig2image(Dxx,Dxy,Dyy);

% figure; imagesc(image_range); colormap(gray)
% 
% figure; imagesc(  l2.*255./max(l2(:))); colormap(gray)
% 
% figure; imagesc(imcomplement (l2 + l1)); colormap(gray)

l2l1_combined = (imcomplement (l2 + l1));
%figure; imagesc(double(image_range) + l2.*255./max(l2(:))); colormap(gray)
% 
% glandIndx = find(  l2(:).*255./max(l2(:)) < 0);
% figure; imagesc(imcomplement(l2(glandIndx) + l1(glandIndx)))
l2l1_combined(1:20, :) = 0;
l2l1_combined(400:end, :) = 0;
[xymax, smax, xymin, smin] = extrema2(l2l1_combined);

[xind, yind] = (ind2sub(size(l2l1_combined), smax));

ind_thresholded = find(xymax > 1.1);


%figure; imagesc(image_range_flat_unpadded); colormap(gray); hold on; plot(yind(1:20), xind(1:20), 'r*')
GlandCenter(frames).x = xind(ind_thresholded) + BalloonLoc(yind(ind_thresholded), frames);
GlandCenter(frames).y = yind(ind_thresholded);

GlandCenterCurr.x = xind(ind_thresholded) + BalloonLoc(yind(ind_thresholded), frames);
GlandCenterCurr.y = yind(ind_thresholded);

figure; imagesc(image_range); colormap(gray); hold on; plot(yind(ind_thresholded) , xind(ind_thresholded) + BalloonLoc(yind(ind_thresholded), frames), '*')


% title(strcat('frame: ', int2str(frames)));
% saveas(gcf, strcat('frame', int2str(frames), '.tif'));
% close all;
% 
% save(strcat('GlandCenter_Frame_', int2str(frames)), 'GlandCenterCurr');

% glandImage = zeros(size(image_range_flat_unpadded));
% for numpts = 1:20
%     currGimage = regiongrowing(l2,xind(numpts), yind(numpts), 0.2);
%     glandImage = glandImage + currGimage;
%     
% end
