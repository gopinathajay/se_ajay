function outImage = UnFlattenImage(inImage, balloonLocation)

%outImage = zeros(size(inImage));

numRows = size(inImage,1);
numCols = size(inImage,2);

if(ndims(inImage) >2)
    numFrames = size(inImage,3);
else
    numFrames = 1;
end

%numFrames = size(balloonLocation,2);

for n=1:numFrames
    
    for i=1:numCols
        
        if(ndims(inImage)>2)
            outImage(:,i,n) = circshift(inImage(:,i,n), balloonLocation(i,n));
        else
            outImage(:,i) = circshift(inImage(:,i), balloonLocation(i,n));
            %outImage(balloonLocation(i,n), i) = 100; %debugging to
            %visualize balloon
        end
    end
    
end