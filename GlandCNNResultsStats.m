[OutfilesRaw] = rdir('\\ImgProcRAID\DataRAID\CAD0examples\RawScanData\*\**\*\*.img');
[OutfilesHypo] = rdir('\\ImgProcRAID\DataRAID\CAD0examples\CNNsegmentationData_20160614\*\**\*\*_HypoScattering.tif');
[OutfilesEnface] = rdir('\\ImgProcRAID\DataRAID\CAD0examples\EnfaceImages\*\**\*\EnfaceSurfaceAttenuation.tif');


load allStatsAllGlands_v2.mat;

thresholdStats.Solidity = mean_Solidity - std_Soliditiy;
thresholdStats.Eccentricity = mean_Eccentricity - std_Eccentricity;
thresholdStats.Circularity = mean_Circularity - std_Circularity;
thresholdStats.Waviness = mean_Waviness - std_Waviness;

countval = 1;
for a=1:size(OutfilesRaw,1)
    if(OutfilesRaw(a).bytes>8388608)
        OutfilesRawValid{countval} = OutfilesRaw(a).name;   
        countval = countval+1;
    end
end

for caseId = 2:countval-1   
 %   if caseId == 49 
 %       continue;
 %   end
%for caseId = 49       
FilePath = OutfilesRawValid{caseId};

objScanData = ScanDataClass_WholeScan(FilePath);

LoadNPMMFile(objScanData);

BalloonLoc = objScanData.BalloonLocation;

objScanDataHypo = ScanDataClass_WholeScan(OutfilesHypo(caseId).name);

objScanDataHypo.LoadTifStack;

S = OutfilesHypo(caseId).name;
FilePathHypoSolidityOutliers = strcat(S(1:end-4), '_OutliersSolidity.tif');
FilePathHypoEccentricityOutliers = strcat(S(1:end-4), '_OutliersEccentricity.tif');
FilePathHypoWavinessOutliers = strcat(S(1:end-4), '_OutliersWaviness.tif');




disp(caseId)
% matfiles=s.mat;
% cd(s.path);
for frNum=1:objScanData.nFramesInScan

% load(char(matfiles(frNum)));

image_range = ReadFramesInRange(frNum, objScanData);

imageLabeled = objScanDataHypo.ScanData(:,:, frNum) > 128;

imageLabCC = bwlabel(imageLabeled)*10;
arrIntensity = unique(imageLabCC(find(imageLabCC> 0)));


%imageLab = zeros(ImgParams.nSamplesPerAline, ImgParams.nAlinesPerFrame);
%imageLab(sub2ind(size(imageLab), Labels.Depth(:), Labels.Aline(:))) = 1;

if objScanData.nAlinesPerFrame == 4096
    % full scan
    unFlatImageLab = UnFlattenImage(imageLabCC, round(objScanData.BalloonLocation(1:4:end,frNum)/4) );
else
    %scout scan
    unFlatImageLab = UnFlattenImage(imageLabCC, round(objScanData.BalloonLocation(1:2:end,frNum)/2) );
end



scanConv = ScanConvert(unFlatImageLab, 1000, 0.25, 1);

%[stats, statImage] = GetStatInfo(scanConv);
[stats, statImage] = GetStatInfoThresholdOutliers(scanConv, thresholdStats);

arrIntensityOutlier.Solidity = unique(round(statImage.Solidity(find(statImage.Solidity> 0))));
arrIntensityOutlier.Waviness = unique(round(statImage.Waviness(find(statImage.Waviness> 0))));
arrIntensityOutlier.Eccentricity = unique(round(statImage.Eccentricity(find(statImage.Eccentricity> 0))));

ScanDataSolidity(:,:,frNum) = uint8(ismember(imageLabCC, arrIntensityOutlier.Solidity));
ScanDataWaviness(:,:,frNum) = uint8(ismember(imageLabCC, arrIntensityOutlier.Waviness));
ScanDataEccentricity(:,:,frNum) = uint8(ismember(imageLabCC, arrIntensityOutlier.Eccentricity));
% 
% OutlierImage(frNum).Solidity = ismember(imageLabCC, arrIntensityOutlier.Solidity);
% OutlierImage(frNum).Waviness = ismember(imageLabCC, arrIntensityOutlier.Waviness);
% OutlierImage(frNum).Eccentricity = ismember(imageLabCC, arrIntensityOutlier.Eccentricity);


% 
% imwrite(OutlierImage(frNum).Solidity, , 'writemode', 'append');
% imwrite(OutlierImage(frNum).Eccentricity, strcat(S(1:end-4), '_OutliersEccentricity.tif'), 'writemode', 'append');
% imwrite(OutlierImage(frNum).Waviness, strcat(S(1:end-4), '_OutliersWaviness.tif'), 'writemode', 'append');


% imwrite(uint16(fliplr(statImage.Waviness*4 + scanConv)), strcat('Waviness_v3', '.tif'), 'writemode', 'append');
% imwrite(uint16(fliplr(statImage.Solidity*4 + scanConv)), strcat('Solidity_v3', '.tif'), 'writemode', 'append');


%[stats, statImage] = GetStatInfo(unFlatImageLab);
% PolarStatImage(frNum).Solidity  = ImToPolar(statImage.Solidity, 0, 1, 512, 1024);
% PolarStatImage(frNum).Eccentricity = ImToPolar(statImage.Eccentricity, 0, 1, 512, 1024);
% PolarStatImage(frNum).NonConvexArea = ImToPolar(statImage.NonConvexArea, 0, 1, 512, 1024);
% 
% enFaceImage.Solidity(frNum,:) = sum(PolarStatImage(frNum).Solidity, 1);
% enFaceImage.Eccentricity(frNum,:) = sum(PolarStatImage(frNum).Eccentricity, 1);
% enFaceImage.NonConvexArea(frNum,:) = sum(PolarStatImage(frNum).NonConvexArea, 1);
end

HypoDir = OutfilesHypo(caseId).name;
HypoDir = HypoDir(1:end-75); % to get the directory

EnfaceDir = OutfilesEnface(caseId).name;
EnfaceDir = EnfaceDir(1:end-28); % to ignore "surface attenuation.tif"

% Solidity
objScanDataHypo.FilePath = FilePathHypoSolidityOutliers;
objScanDataHypo.ScanData = ScanDataSolidity;
objScanDataHypo.ExportDataToTifStack(HypoDir, 'LZW', 1);
EnfaceImageSolidity = flipud(GenerateEnfaceVolumeRayCasting(objScanDataHypo, 4.0, 0.9));
imwrite(double(EnfaceImageSolidity/(max(EnfaceImageSolidity(:)))), strcat(EnfaceDir, 'EnfaceHypoScattering_Outlier_Solidity.tif'));

%Waviness
objScanDataHypo.FilePath = FilePathHypoWavinessOutliers;
objScanDataHypo.ScanData = ScanDataWaviness;
objScanDataHypo.ExportDataToTifStack(HypoDir, 'LZW', 1);
EnfaceImageWaviness = flipud(GenerateEnfaceVolumeRayCasting(objScanDataHypo, 4.0, 0.9));
imwrite(double(EnfaceImageWaviness/(max(EnfaceImageWaviness(:)))), strcat(EnfaceDir, 'EnfaceHypoScattering_Outlier_Waviness.tif'));


%Eccentricity
objScanDataHypo.FilePath = FilePathHypoWavinessOutliers;
objScanDataHypo.ScanData = ScanDataEccentricity;
objScanDataHypo.ExportDataToTifStack(HypoDir, 'LZW', 1);
EnfaceImageEccentricity = GenerateEnfaceVolumeRayCasting(objScanDataHypo, 4.0, 0.9);
imwrite(double(EnfaceImageEccentricity/(max(EnfaceImageEccentricity(:)))), strcat(EnfaceDir, 'EnfaceHypoScattering_Outlier_Eccentricity.tif'));


end


threshArea = 10;

countAllGlands = 1;
for i=1:20
    for j=1:1200
        temp= statsAllFrames(i,j).stats.NonConvexArea;
        tempA = statsAllFrames(i,j).stats.Area;
        for cj=1:length(temp)
            if tempA(cj) < threshArea
                continue;
            end
            stat_NonConvexAllGlandsAllCases(countAllGlands) = temp(cj);
            countAllGlands = countAllGlands+1;
        end
    end
end


countAllGlands = 1;
for i=1:20
    for j=1:1200
        temp= statsAllFrames(i,j).stats.Eccentricity;
        tempA = statsAllFrames(i,j).stats.Area;
        for cj=1:length(temp)
            if tempA(cj) < threshArea
                continue;
            end
            stat_EccentricityAllGlandsAllCases(countAllGlands) = temp(cj);
            countAllGlands = countAllGlands+1;
        end
    end
end

countAllGlands = 1;
for i=1:size(statsAllFrames,1)
    
    for j=1:1200
    
     if isempty(statsAllFrames(i,j).stats) == 1
         continue;
     end
        temp= statsAllFrames(i,j).stats.Solidity;
        tempRnd = statsAllFrames(i,j).stats.Roundness;
        tempWav = statsAllFrames(i,j).stats.Waviness;
        tempEcc = statsAllFrames(i,j).stats.Eccentricity;
        
        tempA = statsAllFrames(i,j).stats.Area;
        tempPer = statsAllFrames(i,j).stats.Perimeter;
        
        for cj=1:length(temp)
            if tempA(cj) < threshArea
                continue;
            end
            stat_SolidityAllGlandsAllCases(countAllGlands) = temp(cj);
            if(temp(cj) ==1)
                pause1 = 1;
            end
            
            stat_CircularityAllGlandsAllCases(countAllGlands) = min(4*pi*tempA(cj) / ((tempPer(cj) + pi)^2),1);
            stat_WavinessAllGlandsAllCases(countAllGlands) = tempWav(cj);
            stat_EccentricityAllGlandsAllCases(countAllGlands) = tempEcc(cj);
            stat_Area(countAllGlands) = tempA(cj);
            
            countAllGlands = countAllGlands+1;
        end
    end
end

figure; hold on;
dat = [stat_EccentricityAllGlandsAllCases', stat_RoundnessAllGlandsAllCases'];
hist3(dat, [70,70]);
n = hist3(dat, [70,70]);
n1 = n';
n1(size(n,1) + 1, size(n,2) + 1) = 0;
xb = linspace(min(dat(:,1)),max(dat(:,1)),size(n,1)+1);
yb = linspace(min(dat(:,2)),max(dat(:,2)),size(n,1)+1);
h = pcolor(xb,yb,n1);

h.ZData = ones(size(n1)) * -max(max(n));
colormap(hot) % heat map
title('Data Point Density Histogram and Intensity Map');
grid on
view(3);