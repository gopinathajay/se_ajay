function ALineIdx = DetectTissueContactWithBalloon(binMask_flat)

balloonThickness = 20; %pixels
balloonThickness = balloonThickness*1.3; % Tolerance of about 30%
ALineIdx = zeros(size(binMask_flat,2),1);
ALineIdx(find(binMask_flat(balloonThickness, :)>0);