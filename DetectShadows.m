%shadowVec = (1-mean(image_range_flat(100:200,:)));
shadowVec = (1-mean(image_binMask_flat(100:200,:)));
shadowVecAll(:, frames) = shadowVec;

shadowVec = shadowVec.*ALinesContact';


[r, lags] = xcorr(shadowVec, DoubleGauss);
[val, idx] = max(r);
ShadowPeakInTissue(:,frames) = lags(idx) + length(DoubleGauss)/2;


shadowVec(find(shadowVec==0)) = min(shadowVec(:));
shadowVec = shadowVec - min(shadowVec(:));

se = strel('square', 50);
threshSh = mean(shadowVec) + 2*std(shadowVec);
shadow_thresh = shadowVec;
shadow_thresh(find(shadowVec < threshSh)) = 0;

shadowVecPeaks = ((1-medfilt1(shadow_thresh,10)) - imopen(1-medfilt1(shadow_thresh,10), se));

[maxVal, peakShadow2] = max(shadowVecPeaks(padNum:(padNum+numALines)));

if maxVal > 0 %valid detection
    %listofDoubleShadow(:,frames) = unPadPeak(peakShadow2, numALines, padNum);
    listofDoubleShadow(:,frames) = peakShadow2;
else
    listofDoubleShadow(:,frames) = -1;
end


