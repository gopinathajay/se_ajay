close all
clearvars
clc

addpath([cd, '\GraphAlgorithm'])
addpath([cd, '\BalloonSegmentation'])
addpath([cd, '\DataInfo'])


Params.ScaleFactor = 55/255;
Params.nAlines = 4096; % number of alines
%Params.nAlines = 1024; % number of alines
Params.nSamples = 2048; % number of samples per aline
%Params.nSamples = 512; % number of samples per aline
Params.nFrames = 1;
Params.volNum = 1;
%Params.fileName = 'Y:\RegistryData\09-Cornell\09_006_02\15-03-05_11-08-59_9510114\F2\15-03-05_11-13-33_F2_9510114_031220140019.img';
%Params.fileName = 'O:\Tsung-Han Tsai\!AnimalLabProcessing\!FullRes\Esophagus_20mm_Distal\Intensity\IMG_27.pgm';  

%FilePath = '\\IMGPROCRAID\DataRAID\tmpMount\SilverDoubleLineData\20X502AJAY\16-08-31_11-00-36_9530120\F1\16-08-31_11-01-19_F1_9530120_040120130002.img';
%FilePath = '\\IMGPROCRAID\DataRAID\ResearchData\20160928_animal_study\Engineering_Test\Wed_September_28_2016\Scan_Data_10-45-59_am\ImageData.img';
FilePath = '\\IMGPROCRAID\DataRAID\tmpMount\Gen3\Lot 5\20_501\16-09-01_10-47-12_9530120\F4\16-09-01_10-59-52_F4_9530120_040120130002.img';
objScanData = ScanDataClass_WholeScan(FilePath);

LoadNPMMFile(objScanData);


BalloonLoc = objScanData.BalloonLocation;
for IndFrame = 1:1200

    IndFrame = 242;
    
    BalloonSegData = BalloonLoc(:, IndFrame);

    
    %% Run Reg Line Detection
    frames = IndFrame;
    padNum = 100;
    image_range = ReadFramesInRange(frames, objScanData);
    numALines = Params.nAlines;
    
    image_range_flat_unpadded = FlattenImage(image_range, BalloonSegData);
    
    image_range_flat = [image_range_flat_unpadded(:,end-padNum+1:end), image_range_flat_unpadded, image_range_flat_unpadded(:,1:padNum)];
    
    vec1 = medfilt1(mean(image_range_flat(1:25,:)),10);
    
    vec1_grad = mean(image_range_flat(25:75)) - mean(image_range_flat(75:125)); 
    
    vec1  = vec1 + vec1_grad;
    
%     %Saturation Correction
%     image_range_insideBalloon_fl_unpad = FlattenImage(image_range,BalloonSegData-250);
% 
%     image_range_insideBalloon_fl_pad = [image_range_insideBalloon_fl_unpad(:,end-padNum+1:end), image_range_insideBalloon_fl_unpad, image_range_insideBalloon_fl_unpad(:,1:padNum)];
% 
%     vec_inB =medfilt1(mean(image_range_insideBalloon_fl_pad(1:250,:)),10);  
% 
%     vec1 = vec1 - double(vec_inB);
%     %
%     
%     vec1_shadow = medfilt1(mean(image_range_flat(200:215,:)),10);
%     
    vecAllFrames(:,frames) = vec1;
    
    thresh = mean(vec1(padNum:(padNum+numALines))) + 3*std(vec1(padNum:(padNum+numALines)));
    
    vec1thresh  = zeros(length(vec1),1)';
    vec1thresh(vec1>thresh) = vec1(vec1>thresh);
    
    se = strel('square', 50);
    vecDoublePeaks = ((1-medfilt1(vec1thresh,10)) - imopen(1-medfilt1(vec1thresh,10), se));
    
    outVec2 = (vecDoublePeaks(padNum:(padNum+numALines)));
    [maxVal, peakDouble2] = max(outVec2);
    
    if maxVal > 0 %valid detection
        % listofDoubleLine2(:,frames) = unPadPeak(peakDouble2, numALines, padNum);
        peakDouble2 = GetMidPointOfPeak(outVec2, maxVal, peakDouble2);
        
        % THIS IS THE REGLINE LOCATION (A-Line Index
        listofDoubleLine2(:,frames) = peakDouble2;
        
    else
        listofDoubleLine2(:,frames) = -1;
    end
    
    t1 = (imopen(1-medfilt1(vec1thresh,10), se)) < 0;
    
    for k=2:length(t1)
       if(t1(k) == 0)
           t2(k) = 0;
       else
          t2(k) = t1(k);
           t2(k) = t2(k-1) + t2(k);
       end
    end
   
    lengthOfDoubleRegLine(frames) = max(t2);
    
end












