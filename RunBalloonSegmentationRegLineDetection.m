close all
clearvars
clc

addpath([cd, '\GraphAlgorithm'])
addpath([cd, '\BalloonSegmentation'])
addpath([cd, '\DataInfo'])


Params.ScaleFactor = 55/255;
Params.nAlines = 4096; % number of alines
%Params.nAlines = 1024; % number of alines
Params.nSamples = 2048; % number of samples per aline
%Params.nSamples = 512; % number of samples per aline
Params.nFrames = 1;
Params.volNum = 1;
%Params.fileName = 'Y:\RegistryData\09-Cornell\09_006_02\15-03-05_11-08-59_9510114\F2\15-03-05_11-13-33_F2_9510114_031220140019.img';
Params.fileName = 'O:\Tsung-Han Tsai\!AnimalLabProcessing\!FullRes\Esophagus_20mm_Distal\Intensity\IMG_27.pgm';  

fid = fopen(Params.fileName,'r','l');


Params.DSfactor = 1;

Params.graphMaxStep = [2, 2, 6];
Params.ModType = [2, 3, 3];
Params.modificationLabels = {'BaseAlgo+NFmod', 'BaseAlgo+NFmod+LINEmod', 'IncreasedMaxStep'};

Params.autoBgndSub = 'n';

% Parameters for Gaussian smoothing and gradient calculation
Params.GaussKernelSize = 20/Params.DSfactor;      
Params.GaussSigma = 5/3;        
Params.GradKernelSize = 1;          

% Modification #1: Parameters for Increasing Noise floor cost
Params.GradientThresh = .2;
Params.AdditionalCost = 40;

% Modification #2: Parameters for Line Enhancement
Params.ImageThresh = 5;
Params.ImageShiftLine = round(-25/Params.DSfactor);


Params.LineMergeSigma = 4;


f1 = 0; 
f2 = 0; 

options.plotEachFrame = 'n';
options.plotEachFrameHandle = f1;

options.plotErrorMets = 'n';
options.plotErrorMetsHandle = f2;
            
options.RunAllCalculations = 'y';

options.PlotLineMerging = 'n';
options.PlotLineMergingHandle1 = f2;
options.PlotLineMergingHandle2 = f2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the thresholds to evalute the error metrics
ErrorMetricThresholds;

% Initialize pre-processing object
objPreProcess = Class_PreProcess(Params);

h = waitbar(0,'');

Params.nMods = length(Params.graphMaxStep);

objResults = Class_Results(Params);
objErrMets = Class_ErrorMetrics(Params, Thresh, f2);
objResults.AdvancedErrorMetricLabels = objErrMets.AdvancedErrorMetricLabels;
        

% Run the Balloon Segmentation on each frame in the data set and plot results
for IndFrame = 1:Params.nFrames
  %  waitbar(IndFrame/Params.nFrames, h, ['Frame ', num2str(IndFrame), ' of ', num2str(Params.nFrames)]);
    
    % Load the frame
    %[RawImg, count] = fread(fid, [Params.nSamples, Params.nAlines],'*uint8');
    [RawImg, count] = fread(fid, [Params.nAlines, Params.nSamples],'*uint8');
    RawImg = RawImg';
    
    % Run the balloon segmentation
    F_BalloonSegmentation(RawImg, Params, objPreProcess, objResults, objErrMets, IndFrame, options);
    
    BalloonSegData = squeeze(objResults.BalloonDepth_Raw(1, 4, :));

    
    %% Run Reg Line Detection
    frames = IndFrame;
    padNum = 100;
    image_range = RawImg;
    numALines = Params.nAlines;
    
    image_range_flat_unpadded = FlattenImage(image_range, BalloonSegData);
    
    image_range_flat = [image_range_flat_unpadded(:,end-padNum+1:end), image_range_flat_unpadded, image_range_flat_unpadded(:,1:padNum)];
    
    vec1 = medfilt1(mean(image_range_flat(1:25,:)),10);
    
    %Saturation Correction
    image_range_insideBalloon_fl_unpad = FlattenImage(image_range,BalloonSegData-250);

    image_range_insideBalloon_fl_pad = [image_range_insideBalloon_fl_unpad(:,end-padNum+1:end), image_range_insideBalloon_fl_unpad, image_range_insideBalloon_fl_unpad(:,1:padNum)];

    vec_inB =medfilt1(mean(image_range_insideBalloon_fl_pad(1:250,:)),10);  

    vec1 = vec1 - double(vec_inB);
    %
    
    vec1_shadow = medfilt1(mean(image_range_flat(200:215,:)),10);
    
    vecAllFrames(:,frames) = vec1;
    
    thresh = mean(vec1(padNum:(padNum+numALines))) + 3*std(vec1(padNum:(padNum+numALines)));
    
    vec1thresh  = zeros(length(vec1),1)';
    vec1thresh(vec1>thresh) = vec1(vec1>thresh);
    
    se = strel('square', 50);
    vecDoublePeaks = ((1-medfilt1(vec1thresh,10)) - imopen(1-medfilt1(vec1thresh,10), se));
    
    outVec2 = (vecDoublePeaks(padNum:(padNum+numALines)));
    [maxVal, peakDouble2] = max(outVec2);
    
    if maxVal > 0 %valid detection
        % listofDoubleLine2(:,frames) = unPadPeak(peakDouble2, numALines, padNum);
        peakDouble2 = GetMidPointOfPeak(outVec2, maxVal, peakDouble2);
        
        % THIS IS THE REGLINE LOCATION (A-Line Index
        listofDoubleLine2(:,frames) = peakDouble2;
        
    else
        listofDoubleLine2(:,frames) = -1;
    end
   
end












