clear

FilePath  = 'T:\AMCdata\Gen2L_FIH\PatientData\GEN2LFIM_12\16-02-22_11-01-01_9530120\F3\16-02-22_11-16-10_F3_9530120_121220130018.img';

objScanData = ScanDataClass_WholeScan(FilePath);

LoadNPMMFile(objScanData);


BalloonLoc = objScanData.BalloonLocation;

padNum = 100;
numFramesProcessed = 1;
%for frames = [1:objScanData.nFramesInScan]
%for frames = 404
for frames = 350:450

    disp(frames)
    
    image_range = ReadFramesInRange(frames, objScanData);
    
    numALines = objScanData.nAlinesPerFrame;
    
    %pad by 50 A-lines on either side
    
    image_range_flat_unpadded = FlattenImage(image_range, objScanData.BalloonLocation(:,frames));
    
    image_range_flat = [image_range_flat_unpadded(:,end-padNum+1:end), image_range_flat_unpadded, image_range_flat_unpadded(:,1:padNum)];
    
    image_binMask = GenBinaryTissueMask(image_range, objScanData.BalloonLocation(:,frames));
    
    image_binMask_flat_unpadded = FlattenImage(image_binMask, objScanData.BalloonLocation(:,frames));
    
    image_binMask_flat = [image_binMask_flat_unpadded(:,end-padNum+1:end), image_binMask_flat_unpadded, image_binMask_flat_unpadded(:,1:padNum)];
    
    ALinesContact = DetectTissueBalloonContact(image_binMask_flat);
    
    ALineContactAllFrames(:,frames) = ALinesContact';

    DetectGlands;
    
    numFramesProcessed =numFramesProcessed +1;
end


