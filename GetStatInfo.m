function [stats, statImage]  = GetStatInfo(inputBinImage)

inputBinImage(isnan(inputBinImage)) = 0;

inputBinImage = logical(inputBinImage);
CC  = bwconncomp(inputBinImage);
stats = regionprops('table', CC, 'centroid', 'MajorAxisLength', 'MinorAxisLength', 'ConvexImage', 'ConvexArea', 'Area', 'Solidity', 'Eccentricity', 'PixelIdxList', 'Perimeter');


stats.NonConvexArea  = stats.ConvexArea - stats.Area;

stats.Roundness = 4*stats.Area*pi./(stats.Perimeter.^2);

convHullPerimeter = [];
for lenComp = 1:size(stats,1)
    if iscell((stats(lenComp,:).ConvexImage))
        convHullPerimeter(lenComp) =  length((bwperim(cell2mat(stats(lenComp,:).ConvexImage)))>0) ;
    else
        convHullPerimeter(lenComp) =  length((bwperim((stats(lenComp,:).ConvexImage)))>0) ;
    end
end
stats.ConvexHullPerimeter = convHullPerimeter';

stats.Waviness = stats.ConvexHullPerimeter./stats.Perimeter; 

statImage.NonConvexArea  = zeros(size(inputBinImage));

statImage.Eccentricity  = zeros(size(inputBinImage));

statImage.Solidity  = zeros(size(inputBinImage));

for i=1:CC.NumObjects
    
statImage.NonConvexArea(CC.PixelIdxList{i}) = stats.NonConvexArea(i);

statImage.Eccentricity(CC.PixelIdxList{i}) = stats.Eccentricity(i);

statImage.Solidity(CC.PixelIdxList{i}) = stats.Solidity(i);

end

