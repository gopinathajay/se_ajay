%% read in a range of frames and put them in a 3D image data structure
function frames = ReadFramesInRange(frameRange, objScanData)

fid = fopen(objScanData.FilePath);

for n = 1:length(frameRange)
    
    %Go to the beginning of the frame
    fseek(fid, (frameRange(n)-1)*objScanData.nSamplesPerAline*objScanData.nAlinesPerFrame, 'bof');
    
    frames(:,:,n) = fread(fid, [objScanData.nSamplesPerAline, objScanData.nAlinesPerFrame], '*uint8');
    
end

fclose(fid);

end