function peak = GetLocalMaxima(d, w, thresh)

d(find(d<thresh)) = 0;

%find local maxima
lmax=[false; (diff(sign(diff(d)))<0); false];
lmaxidx=find(lmax);

%sort local maxima
[~,idx]=sort(-d(lmaxidx));

%remove those closer than w points
ii=1;
while (ii<length(idx))
  
tooclose=find(abs(lmaxidx(idx((ii+1):end))-lmaxidx(idx(ii)))<w)+ii;
   idx(tooclose)=[];
   ii=ii+1;
end

%transform indices in lmaxidx into indices in d
peak=lmaxidx(idx);