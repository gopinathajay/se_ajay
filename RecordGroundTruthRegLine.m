I = enFace;
figure; imagesc(I); colormap(gray);
M = imfreehand(gca,'Closed',0);
F = false(size(M.createMask));
P0 = M.getPosition;
D = ([0; cumsum(sum(abs(diff(P0)),2))]); % Need the distance between points...
P = interp1(D,P0,D(1):.5:D(end)); % ...to close the gaps
P = unique(round(P),'rows');
S = sub2ind(size(I),P(:,2),P(:,1));
F(S) = true;
figure; imshow(F);