%Input image is a flattened A-line binary mask image
%Detects the A-lines in which the balloon is in contact with tissue. 1 for in contact, 0
%for no contact

function ALines = DetectTissueBalloonContact(image_binMask_flat)

nDepth = 40; %walk fifty pixels from the ballon into the image.
% if a background pixel is detected, tag it as a no tissue contact Aline

if(ndims(image_binMask_flat) > 2)
    nFrames = size(image_binMask_flat,3);
else
    nFrames = 1;
end

numCols = size(image_binMask_flat, 2);

ALinesCurr = ones(size(image_binMask_flat, 2),1); %% initialize to all contact

for nFr = 1:nFrames
    
    if nFrames == 1;
        imageCurr = image_binMask_flat; 
    else
        imageCurr = image_binMask_flat(:,:,nFr);
    end
    
    for nc = 1:numCols
        
        for nr = 10:nDepth
            
            if(imageCurr(nr, nc)==0)
                ALinesCurr(nc) = 0;
                break;
            end
            
        end
        
    end
    
    if(ndims(image_binMask_flat) > 2)
        ALines(:,nFr) = ALinesCurr;
    else
        ALines = ALinesCurr;
    end
    
end