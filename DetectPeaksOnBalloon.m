vec1 = medfilt1(mean(image_range_flat(1:15,:)),10);

vec1_shadow = medfilt1(mean(image_range_flat(200:215,:)),10);
%vec1 = vec1 - vec1_shadow;

vecAllFrames(:,frames) = vec1;

enFace(:,frames) = mean(image_range_flat_unpadded(1:25,:));

%vec1_temp = medfilt1(mean(image_range_flat_unpadded(1:15,:)),15);

thresh = mean(vec1(padNum:(padNum+numALines))) + 1*std(vec1(padNum:(padNum+numALines)));

thresh_allFra(frames) = thresh;

pTemp = GetLocalMaxima(vec1', 500, thresh);
%Get only top 4 hits
if(length(pTemp)>4)
    p = pTemp(1:4);
else %% if less than 4, append zeros
    p = [pTemp; -1*ones(4-length(pTemp),1)];
end

for pi = 1:4
    if(p(pi) ~= -1)
        listOfPeaks(pi,frames) = unPadPeak(p(pi), numALines, padNum);
    else
        listOfPeaks(pi,frames) = -1;
    end
end

vec1thresh  = zeros(length(vec1),1)';
vec1thresh(vec1>thresh) = vec1(vec1>thresh);
[r, lags] = xcorr(vec1thresh, DoubleGauss);
[val, idx] = max(r);

listOfDoubleLine(:,frames) = lags(idx) + length(DoubleGauss)/2;
se = strel('square', 50);
%vecDoublePeaks = ((1-medfilt1(vec1thresh,20)) - imopen(1-medfilt1(vec1thresh,20), se));
vecDoublePeaks = ((1-(vec1thresh)) - imopen(1-medfilt1(vec1thresh,20), se));

outVec2 = (vecDoublePeaks(padNum:(padNum+numALines)));
[maxVal, peakDouble2] = max(outVec2);

if maxVal > 0 %valid detection
  % listofDoubleLine2(:,frames) = unPadPeak(peakDouble2, numALines, padNum);
   listofDoubleLine2(:,frames) = peakDouble2;
else
    listofDoubleLine2(:,frames) = -1;
end
vec_invpeaks = (1-medfilt1(vec1thresh,20));
vecDoublePeaksIdx  = vecDoublePeaks > 0;
outVec3 = (vecDoublePeaksIdx(padNum:(padNum+numALines)) .* vec_invpeaks(padNum:(padNum+numALines)));

[maxval, peakdouble3] = max(outVec3);

if maxVal > 0 %valid detection
  % listofDoubleLine2(:,frames) = unPadPeak(peakDouble2, numALines, padNum);
   listofDoubleLine3(:,frames) = peakdouble3;
else
    listofDoubleLine3(:,frames) = -1;
end

outVec4 = ComputeCentralDiff(vec1, find(vecDoublePeaks>0) , 80, padNum, numALines);

[maxval, peakdouble4] = max(outVec4);

if maxVal > 0 %valid detection
  % listofDoubleLine2(:,frames) = unPadPeak(peakDouble2, numALines, padNum);
   listofDoubleLine4(:,frames) = peakdouble4;
else
    listofDoubleLine4(:,frames) = -1;
end


[maxval, peakdouble4_2] = max(outVec4' + outVec2);

if maxVal > 0 %valid detection
  % listofDoubleLine2(:,frames) = unPadPeak(peakDouble2, numALines, padNum);
   listofDoubleLine4_2(:,frames) = peakdouble4_2;
else
    listofDoubleLine4_2(:,frames) = -1;
end