%Build double line profile
sigma = 5;
width = 40;
x = linspace(-width / 2, width / 2, width);
gaussFilter = exp(-x .^ 2 / (2 * sigma ^ 2));
gaussFilter = gaussFilter;% / sum (gaussFilter); % normalize

sigma = 3;
width = 10;
x = linspace(-width / 2, width / 2, width);
gaussFilterDip = exp(-x .^ 2 / (2 * sigma ^ 2));
gaussFilterDip = gaussFilterDip;% / sum (gaussFilterDip); % normalize
DoubleGauss = [gaussFilter'; -1*gaussFilterDip'; gaussFilter'];
%
DoubleGauss(1:5) = -1*gaussFilterDip(6:10)';
DoubleGauss(86:90) = -1*gaussFilterDip(1:5)';
%end double line profile template