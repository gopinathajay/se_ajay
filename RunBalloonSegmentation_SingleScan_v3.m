close all
clearvars
clc

addpath([cd, '\GraphAlgorithm'])
addpath([cd, '\BalloonSegmentation'])
addpath([cd, '\DataInfo'])


Params.ScaleFactor = 55/255;
Params.nAlines = 4096; % number of alines
Params.nSamples = 2048; % number of samples per aline
Params.nFrames = 1;
Params.volNum = 1;
%Params.fileName = 'D:\PHANTOM\15-05-12_13-35-54_9510120\F1\15-05-12_13-37-04_F1_9510120_041020230004.img';
Params.fileName = 'T:\NewRegLineData\SILVER01_F4\Snapshot_F334_A1242\Frame.img';         
Params.DSfactor = 1;

Params.graphMaxStep = [2, 2, 6];
Params.ModType = [2, 3, 3];
Params.modificationLabels = {'BaseAlgo+NFmod', 'BaseAlgo+NFmod+LINEmod', 'IncreasedMaxStep'};

Params.autoBgndSub = 'n';

% Parameters for Gaussian smoothing and gradient calculation
Params.GaussKernelSize = 20/Params.DSfactor;      
Params.GaussSigma = 5/3;        
Params.GradKernelSize = 1;          

% Modification #1: Parameters for Increasing Noise floor cost
Params.GradientThresh = .2;
Params.AdditionalCost = 40;

% Modification #2: Parameters for Line Enhancement
Params.ImageThresh = 5;
Params.ImageShiftLine = round(-25/Params.DSfactor);


Params.LineMergeSigma = 4;


f1 = figure; 
f2 = figure; 

options.plotEachFrame = 'y';
options.plotEachFrameHandle = f1;

options.plotErrorMets = 'n';
options.plotErrorMetsHandle = f2;
            
options.RunAllCalculations = 'y';

options.PlotLineMerging = 'n';
options.PlotLineMergingHandle1 = f2;
options.PlotLineMergingHandle2 = f2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the thresholds to evalute the error metrics
ErrorMetricThresholds;

% Initialize pre-processing object
objPreProcess = Class_PreProcess(Params);

h = waitbar(0,'');

Params.nMods = length(Params.graphMaxStep);

objResults = Class_Results(Params);
objErrMets = Class_ErrorMetrics(Params, Thresh, f2);
objResults.AdvancedErrorMetricLabels = objErrMets.AdvancedErrorMetricLabels;
        
fid = fopen(Params.fileName,'r','l');

% Run the Balloon Segmentation on each frame in the data set and plot results
for IndFrame = 1:Params.nFrames
    waitbar(IndFrame/Params.nFrames, h, ['Frame ', num2str(IndFrame), ' of ', num2str(Params.nFrames)]);
    
    % Load the frame
    [RawImg, count] = fread(fid, [Params.nSamples, Params.nAlines],'*uint8');
    
    % Run the balloon segmentation
    F_BalloonSegmentation(RawImg, Params, objPreProcess, objResults, objErrMets, IndFrame, options);
    
    if options.plotEachFrame == 'y'
        figure(options.plotEachFrameHandle);
        %                 set(options.plotEachFrameHandle, 'Position', [-1500, 200, 1400, 600]);
        
        a1 = subplot(2, 1, 1); imagesc(255-RawImg(1:1:end, 1:1:end)); colormap(gray)
        hold on; plot(1*squeeze(objResults.BalloonDepth_Raw(IndFrame, 1:3, :))'); hold off;
        legend(Params.modificationLabels)
        a2 = subplot(2, 1, 2); imagesc(255-RawImg(1:1:end, 1:1:end)); colormap(gray)
        hold on; plot(1*squeeze(objResults.BalloonDepth_Raw(IndFrame, 4, :))'); hold off;
        
        linkaxes([a1, a2], 'xy')
    end
    
    if options.plotErrorMets == 'y'
        cnames = [Params.modificationLabels, 'MergedLine'];
        rnames = ['Derivative Error Metric', objErrMets.AdvancedErrorMetricLabels, ' ', objErrMets.AdvancedErrorMetricLabels];
        d = [objResults.PassesPrimaryErrorMetric(IndFrame, :); squeeze(objResults.PassesAdvancedErrorMetric(IndFrame, :, :))'; ...
            nan(1, 4); squeeze(objResults.AdvancedErrorMetricValues(IndFrame, :, :))'];
        
        f = options.plotErrorMetsHandle;
        t = uitable(f, 'Data', d, 'ColumnName', cnames, 'RowName', rnames);
        
        set(f, 'Position', [-1500, -300, 1400, 400]);
        figPos = get(f, 'Position');
        set(t, 'position', [20, 20, figPos(3)-40, figPos(4)-40])
    end
    
end

fileFullPath = Params.fileName;
[~, fileName] = fileparts(fileFullPath);

close(h);

BalloonSegData = squeeze(objResults.BalloonDepth_Raw(:, 4, :));




