[OutfilesRaw] = rdir('\\ImgProcRAID\DataRAID\CAD0examples\RawScanData\*\**\*\*.img');
[OutfilesTissue] = rdir('\\ImgProcRAID\DataRAID\CAD0examples\CNNsegmentationData_20160614\*\**\*\*_Tissue_Mask.tif');
[OutfilesEnface] = rdir('\\ImgProcRAID\DataRAID\CAD0examples\EnfaceImages\*\**\*\EnfaceSurfaceAttenuation.tif');
[OutfilesTissueSurf] = rdir('\\ImgProcRAID\DataRAID\CAD0examples\CNNsegmentationData_20160614\*\**\*\TissueSurface.mat');

countval = 1;
for a=1:size(OutfilesRaw,1)
    if(OutfilesRaw(a).bytes>8388608)
        OutfilesRawValid{countval} = OutfilesRaw(a).name;   
        countval = countval+1;
    end
end

for caseId = 49:55;
%for caseId = 2;

FilePath = OutfilesRawValid{caseId};
%FilePathRawCorrected = '\\ImgProcRAID\DataRAID\MetaDataCorrectedScans\AMCcorrelation\AMCcorrelation_MetaDataCorrected_MidRes\02018\14-05-18_22-47-45_9510117\F4\TifStack_14-05-18_23-13-29_F4_9510117_121220130018_Polar.tif';
FileNameTissueSurf = OutfilesTissueSurf(caseId).name;
load(FileNameTissueSurf)
objScanData = ScanDataClass_WholeScan(FilePath);

LoadNPMMFile(objScanData);

BalloonLoc = objScanData.BalloonLocation;

objScanDataTissue = ScanDataClass_WholeScan(OutfilesTissue(caseId).name);

objScanDataTissue.LoadTifStack;

%objRawDataCorrected = ScanDataClass_WholeScan(FilePathRawCorrected);
%objRawDataCorrected.LoadTifStack

%initialize 
vecSurf = [];

for frameNum = 1:objScanData.nFramesInScan
%frameNum = 55;

vecSurf = TissueSurface(frameNum,:);
slopeVec =  (diff(vecSurf));
slopeVec(abs(slopeVec)<25) = 0;

fallingPeak = 0;
risingPeak = 0;
foldsRegion = zeros(size(slopeVec));

for i=1:length(slopeVec)
    
    
    if(abs(slopeVec(i)) > 0)
        
        foundFold_i = i;
        
        while(vecSurf(i) > 50 || vecSurf(i) ==0)
            foldsRegion(i) = 1;
            i = i+1;
            if i > length(slopeVec)
                break;
            end
%             if(slopeVec(i) > 0)
%                 break;
%             end
                
        end
        foundFold_end_i = i;
        
        % fold was found..now look for lack of apposition in the other direction
        i =  foundFold_i; % first restore index
        while(i >= 1 && (vecSurf(i) > 50 || vecSurf(i) ==0))
          foldsRegion(i) = 1;
          i = i-1;
           if i == 1
               break;
            end
                
        end
        
        i = foundFold_end_i;     
    
    end
    
    
end

enFaceFolds(frameNum, :) = foldsRegion(:);



end

% EnfaceDir = OutfilesEnface(caseId).name;
% EnfaceDir = EnfaceDir(1:end-28); % to ignore "surface attenuation.tif"
% imwrite(enFaceFolds, strcat(EnfaceDir, 'EnfaceRugalFolds.tif'));



enFaceFolds_GEN2LFIM16(:,:,caseId) = enFaceFolds;

end

frameNum = 55
figure; 
subplot(2,1,1)
imagesc(objRawDataCorrected.ScanData(:,:,frameNum)); colormap(gray)



hold on; plot(TissueSurface(frameNum,:), 'r')

subplot(2,1,2)
vecSurf = TissueSurface(frameNum,:);
slopeVec = (diff(vecSurf));
slopeVec(abs(slopeVec)<40) = 0;
%plot((slopeVec)); axis tight
plot((foldsRegion)); axis tight



