function peakList = GetPeakListFromVec(inputVec)

maxSortList = sort(inputVec(inputVec ~=0), 'descend');

cc = bwconncomp(inputVec);

for objIdx = 1:cc.NumObjects
    
    indices = cc.PixelIdxList{objIdx};
    
    midPt(objIdx) = indices(round(length(indices)/2));
    midPt_val(objIdx) = inputVec(midPt(objIdx));
end
    
[~, ind] = sort(midPt_val, 'descend');
peakList = midPt(ind);