function binMask = GenBinaryTissueMask(RawImg, BalloonData)

if(ndims(RawImg)==2) %% if it is a single image
    
    minBalloonIdx = round(min(BalloonData) * 0.9);
    
    noiseImage = RawImg(1:minBalloonIdx, :);
    
    nonZeroIndx = find(noiseImage>0);
    meanVal = mean((double(noiseImage(nonZeroIndx))));
    stdVal = std((double(noiseImage(nonZeroIndx))));
    %hist(log(double(noiseImage(nonZeroIndx)+1)))
    SE = strel('square', 10);
    
    noiseThresh = meanVal + 6*stdVal;
   
    se = strel('square',3);
    Img_opened = imopen((double(RawImg) + 1), SE);
    
     maskImage = Img_opened > noiseThresh;
    %figure; imagesc(log(double(RawImg) + 1) > noiseThresh)
    
    binMask = (imclose(imclose(imopen(maskImage, se), se), se));
    


else % array of 2D image stacked in 3D
    
    for nd = 1:size(RawImg,3)
        
        BalloonCurrFrame = BalloonData(:,nd);
        
        RawImgCurrFrame = squeeze(RawImg(:,:,nd));
        
        minBalloonIdx = round(min(BalloonCurrFrame) * 0.9);
        
        noiseImage = RawImgCurrFrame(1:minBalloonIdx, :);
        
        nonZeroIndx = find(noiseImage>0);
        meanVal = mean(log(double(noiseImage(nonZeroIndx))+1));
        stdVal = std(log(double(noiseImage(nonZeroIndx))+1));
        %hist(log(double(noiseImage(nonZeroIndx)+1)))
        
        noiseThresh = meanVal + 2*stdVal;
        maskImage = log(double(RawImgCurrFrame) + 1) > noiseThresh;
        %figure; imagesc(log(double(RawImg) + 1) > noiseThresh)
        se = strel('square',3);
        binMaskCurrFrame = (imclose(imclose(imopen(maskImage, se), se), se));
        binMask(:,:,nd) = binMaskCurrFrame;
        
    end
    
    
end
