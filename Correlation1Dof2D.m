function r = Correlation1Dof2D(binMask_flat_shift_trunc, binMask_flat_trunc)

a = binMask_flat_shift_trunc;
b = binMask_flat_trunc;

r = zeros(1,size(a,2));
tmp = b;
stepSize = 1;
for k = 1:stepSize:size(a,2)

C = bsxfun(@times, a, tmp); %element by element multiplication
r(k) = sum(C(:));
%r(k) = a(:)'*tmp(:);
%circ shift
% for i=1:size(a,1)
% v = tmp(i,:);

tmp  = circshift(tmp, [0, stepSize]);

% tmp(i,:) = v_s;
% end

%tmp = [0 tmp(1:end-1)];
end


