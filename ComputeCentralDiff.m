function out = ComputeCentralDiff(inputVec, indx, windowSize, padNum, numALines)

out = zeros(length(inputVec),1);

for i=1:length(indx)

    if(indx(i) <=padNum || indx(i) >= length(inputVec) - padNum)
        continue;
    end
    leftSet = mean(inputVec(indx(i) : indx(i)+ windowSize));
    rightSet = mean(inputVec(indx(i)- windowSize : indx(i)));
    
    out(indx(i)) = ((leftSet + rightSet)/2 -  inputVec(indx(i)));%/ abs(leftSet - rightSet) ;
    
end
out = out(padNum:padNum+numALines);