%inputImage = double(invAttenCoeff);
inputImage = double(imcomplement(image_range_flat(1:400,:)));

[numro, numcol] = size(inputImage);

%sizeFactor = 10;
sizeFactor = 20;

inputImage_resize = imresize(inputImage, [numro, round(numcol/sizeFactor)]);

%inputImage_resize = Img1;

%[kp, cmax] = Sift2D_ajay(inputImage_resize, 2, 2); % good for detecting
%large cavity that's very distinct
[kp, cmax] = Sift2D_ajay(inputImage_resize, 2, 4); % good for detecting most cavities

%[kp, cmax] = Sift2D_ajay(inputImage_resize, 1, 4); % good for detecting most cavities
% figure; imagesc(inputImage_resize); colormap(gray);
% hold on; plot(uint16(kp(:,2)), uint16(kp(:,1)), 'r*')
if(cmax > 0)
    linearInd = sub2ind(size(inputImage_resize), uint16(kp(:,1)), uint16(kp(:,2)));
    
    siftPoints = zeros(size(inputImage_resize));
    siftPoints(linearInd) = 1;
    
    siftPoint_resize = imresize(siftPoints, [numro, numcol]);
    
    siftPoint_resize(find(siftPoint_resize<0)) = 0;
    
    siftVec = int8(sum(siftPoint_resize,1));
    [~, IndPts, vals] = find(siftVec>0);
    
    %IndPts_unpad = unPadPeak(IndPts, numALines, padNum);
    IndPts_unpad = arrayfun(@unPadPeak, IndPts, numALines*(ones(size(IndPts))), padNum*(ones(size(IndPts))));
    
    siftVecUnPad = zeros(1,numALines);
    siftVecUnPad(IndPts_unpad) = siftVec(IndPts);
    enFaceGlandCount(:, frames) = siftVecUnPad;
    
else
    enFaceGlandCount(:, frames) = 0;
end
