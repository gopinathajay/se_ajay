
clear

%FilePath  = '\\imgprocRAID\DataRAID\AMCdata\Gen2L_FIH\PatientData\GEN2LFIM_12\16-02-22_11-01-01_9530120\F3\16-02-22_11-16-10_F3_9530120_121220130018.img';

%first hand scan from dane%
%FilePath  = 'C:\Data\MICRO_10\16-03-03_14-27-34_BalloonSize20MM\F1\16-03-03_14-27-48_F1_BalloonSize20MM_032520130001.img';

%Scattering phantom
%FilePath = 'C:\Data\MICROPEN18_\16-03-10_15-35-23_BalloonSize20MM\F2\16-03-10_15-40-32_F2_BalloonSize20MM_032520130001.img';

%phantom layered
%FilePath = 'C:\Data\MICROPEN18_\16-03-10_15-35-23_BalloonSize20MM\F1\16-03-10_15-37-33_F1_BalloonSize20MM_032520130001.img';

%hand scan
%FilePath = 'T:\tmpMount\MICROPEN18\16-03-10_14-59-28_BalloonSize20MM\F3\16-03-10_15-07-32_F3_BalloonSize20MM_032520130001.img';


%75% loading
%FilePath = 'C:\Data\MICRO_07\16-03-03_15-37-43_BalloonSize20MM\F1\16-03-03_15-38-03_F1_BalloonSize20MM_032520130001.img';


%50% loading
%FilePath = 'C:\Data\MICRO_04\16-03-03_14-23-23_BalloonSize20MM\F1\16-03-03_14-23-37_F1_BalloonSize20MM_032520130001.img';

%25% loading
%FilePath = 'C:\Data\MICRO_01\16-03-03_13-44-59_BalloonSize20MM\F1\16-03-03_13-46-50_F1_BalloonSize20MM_032520130001.img';

%FilePath = '\\IMGPROCRAID\DataRAID\tmpMount\EXVIVO_1_S1\16-04-05_13-39-36_BalloonSize20MM\F4\16-04-05_13-53-28_F4_BalloonSize20MM_unknown.img'

%FilePath = '\\IMGPROCRAID\DataRAID\tmpMount\TEST\16-05-18_12-08-42_9510120\F1\16-05-18_12-16-15_F1_9510120_082420150001.img';

%FilePath = 'C:\Data\SILVER1\16-06-02_09-08-51_9510120\F1\16-06-02_09-09-11_F1_9510120_9999.img';

FilePath = '\\IMGPROCRAID\DataRAID\tmpMount\SilverRegLineData\SILVER1_saturation_Motion\16-06-02_09-08-51_9510120\F1\16-06-02_09-09-11_F1_9510120_9999.img';
FilePath = '\\IMGPROCRAID\DataRAID\tmpMount\SilverDoubleLineData\20X502AJAY\16-08-31_11-00-36_9530120\F1\16-08-31_11-01-19_F1_9530120_040120130002.img'
objScanData = ScanDataClass_WholeScan(FilePath);

LoadNPMMFile(objScanData);


BalloonLoc = objScanData.BalloonLocation;


BuildIntensityTemplate

padNum = 100;
numFramesProcessed = 1;


figure; 
for frames = [1:objScanData.nFramesInScan]
%for frames = 367
%for frames = 110;
    disp(frames)
    
    image_range = ReadFramesInRange(frames, objScanData);
    
    %image_range = f_ComputeAttenCoeffImage(image_range, 6);
    
    numALines = objScanData.nAlinesPerFrame;
    
    %pad by 50 A-lines on either side
    
    image_range_flat_unpadded = FlattenImage(image_range, objScanData.BalloonLocation(:,frames));
    
    image_range_flat = [image_range_flat_unpadded(:,end-padNum+1:end), image_range_flat_unpadded, image_range_flat_unpadded(:,1:padNum)];
    
    image_binMask = GenBinaryTissueMask(image_range, objScanData.BalloonLocation(:,frames));
    
    image_binMask_flat_unpadded = FlattenImage(image_binMask, objScanData.BalloonLocation(:,frames));
    
    image_binMask_flat = [image_binMask_flat_unpadded(:,end-padNum+1:end), image_binMask_flat_unpadded, image_binMask_flat_unpadded(:,1:padNum)];
    
    DetectPeaksOnBalloon_bkup_v2
    continue;
%         
    ALinesContact = DetectTissueBalloonContact(image_binMask_flat);
    
    ALineContactAllFrames(:,frames) = ALinesContact';
        
        
    DetectShadows;
%     
    listofDoubleLine2_noTracking = listofDoubleLine2;
    
    GetConfidenceForDoubleLineDetect;
       
    numFramesProcessed =numFramesProcessed +1;
    
%     imagesc(enFace); colormap(gray)
%     hold on; plot(listofDoubleLine4_2, 'g*')
end

figure; imagesc(enFace); colormap(gray)
hold on; plot(listofDoubleLine2, 'r*')
hold on; plot(listofDoubleShadow, 'b*')

figure; imagesc(enFace); colormap(gray)
hold on; plot(listofDoubleLine2_noTracking, 'r*')
hold on; plot(listofDoubleLine2, 'g*')

figure; imagesc(enFace); colormap(gray)
for i=1:4
hold on; plot(1:size(listOfPeaks,2), listOfPeaks(i,:), 'r*')
end

p = redgreencolormap_1([], 'interpolation', 'linear');
p_m = [p(:,2), p(:,1), p(:,3)];
figure; imagesc(doubleLineConfidence); colormap(p_m)

