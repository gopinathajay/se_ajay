function [stats, statImage]  = GetStatInfoThresholdOutliers(inputBinImageIntensity, vargin)

if nargin == 1
    ThreshStatsPresent = false;
else
    ThreshStatsPresent = true;
    thresholdStats = vargin(2);
end

inputBinImage = inputBinImageIntensity;
inputBinImage(isnan(inputBinImageIntensity)) = 0;
inputBinImage(find(inputBinImage > 0)) = 1;

inputBinImage = logical(inputBinImage);
CC  = bwconncomp(inputBinImage);
stats = regionprops('table', CC, 'centroid', 'MajorAxisLength', 'MinorAxisLength', 'ConvexImage', 'ConvexArea', 'Area', 'Solidity', 'Eccentricity', 'PixelIdxList', 'Perimeter');


stats.NonConvexArea  = stats.ConvexArea - stats.Area;

stats.Roundness = 4*stats.Area*pi./(stats.Perimeter.^2);

convHullPerimeter = [];
for lenComp = 1:size(stats,1)
    if iscell((stats(lenComp,:).ConvexImage))
        convHullPerimeter(lenComp) =  length((bwperim(cell2mat(stats(lenComp,:).ConvexImage)))>0) ;
    else
        convHullPerimeter(lenComp) =  length((bwperim((stats(lenComp,:).ConvexImage)))>0) ;
    end
end
stats.ConvexHullPerimeter = convHullPerimeter';

stats.Waviness = stats.ConvexHullPerimeter./stats.Perimeter; 

statImage.NonConvexArea  = zeros(size(inputBinImage));

statImage.Eccentricity  = zeros(size(inputBinImage));

statImage.Solidity  = zeros(size(inputBinImage));

statImage.Waviness  = zeros(size(inputBinImage));

statImage.SolidityLabel = zeros(size(inputBinImage));
statImage.SolidityInvLabel = zeros(size(inputBinImage));
statImage.AreaLabel = zeros(size(inputBinImage));
statImage.AreaInvLabel = zeros(size(inputBinImage));
statImage.DepthLabel = zeros(size(inputBinImage));

for i=1:CC.NumObjects
    
    % if thresholds are available on each statistical metric, use it here
    if ThreshStatsPresent == true
        if(table2array(stats(i,{'Eccentricity'})) < thresholdStats.Eccentricity)
            statImage.Eccentricity(CC.PixelIdxList{i}) = round(max(inputBinImageIntensity(CC.PixelIdxList{i})));
        end
        
        if(table2array(stats(i,{'Solidity'})) < thresholdStats.Solidity)
            statImage.Solidity(CC.PixelIdxList{i}) = round(max(inputBinImageIntensity(CC.PixelIdxList{i})));
        end
        
        if(table2array(stats(i,{'Waviness'})) < thresholdStats.Waviness)
            statImage.Waviness(CC.PixelIdxList{i}) = round(max(inputBinImageIntensity(CC.PixelIdxList{i})));
        end
        
    else
        
        statImage.NonConvexArea(CC.PixelIdxList{i}) = stats{i, 'NonConvexArea'}; 

        statImage.Eccentricity(CC.PixelIdxList{i}) = stats{i, 'Eccentricity'};

        statImage.Solidity(CC.PixelIdxList{i}) = stats{i, 'Solidity'};
        
    end
    
     statImage.SolidityLabel(CC.PixelIdxList{i}) = table2array(stats(i,{'Solidity'}));
     statImage.SolidityInvLabel(CC.PixelIdxList{i}) = 1-table2array(stats(i,{'Solidity'}));
     statImage.AreaLabel(CC.PixelIdxList{i}) = length(CC.PixelIdxList{i});
     statImage.AreaInvLabel(CC.PixelIdxList{i}) = 1/length(CC.PixelIdxList{i});
     
     
     
     
end


