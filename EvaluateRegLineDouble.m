%Evaluate performance
function [perf, AllDetection] = EvaluateRegLineDouble(FrameNumber,ALine , listDoubleLineDetect, numALines, varargin)


%load(groundTruth)


nStartFrame = FrameNumber(1)+1;
nEndFrame = FrameNumber(end)+1;

limiting = 0;

if length(varargin) >= 2
    nStartFrame_limit = varargin{1};
    nEndFrame_limit = varargin{2};
    limiting = 1;
    
end

if length(varargin) == 1
    nEndFrame_limit = varargin{1};
    limiting = 1;
end

ALineTolerance = 25; %around 2 degrees is 20 ALines and ground truth tolerance is about 10 A-lines (gap between the two peaks). Hence 25 Alines was chosen as tolerance 
countDetected = 0;
countMissed = 0;
countNoDetections = 0;

frameIdx = 0;

countValidFrames = 0;

for fr = nStartFrame:nEndFrame
    
    frameIdx = frameIdx + 1;
    
    if(limiting == 1)
        if(fr > nEndFrame_limit)
            continue;
        end
        
        if(fr < nStartFrame_limit)
            continue;
        end
    end
    
    countValidFrames = countValidFrames + 1;
    
    detectALine = listDoubleLineDetect(fr);
    gtALine = ALine(frameIdx)+1;
    
    if detectALine == -1
        detectionSuccess(fr) = -1; %False Negative
        countNoDetections = countNoDetections + 1;
        continue;
    end
    
    
    if ( abs(gtALine - detectALine) <= ALineTolerance || ...
       abs(gtALine + numALines - detectALine) <= ALineTolerance || ...
       abs(gtALine - numALines - detectALine) <= ALineTolerance ) 
       
        detectionSuccess(fr) = 1; % True Positive
        countDetected = countDetected+1;
        
    else
        
        detectionSuccess(fr) = 0; % False Positive
        countMissed = countMissed + 1;
        
    end
    
    AllDetection.GTALine(countValidFrames) = gtALine;
    AllDetection.DetectALine(countValidFrames) = detectALine;
    AllDetection.currFrame(countValidFrames) = fr;
    
end

perf.detectionSuccess = detectionSuccess;
perf.Sensitivity = countDetected / countValidFrames;
perf.PPV = countDetected / (countDetected + countMissed);

perf.FailureRate = countNoDetections/countValidFrames;

%PPV and sensitivity will be different if there are cases where the
%algorithm did not detect at all


figure; plot(ALine, '*'); hold on; plot( listDoubleLineDetect(FrameNumber), 'r*'); legend('Ground Truth', 'Detection'); grid on; xlabel('Frame Index'); ylabel('A-line')
