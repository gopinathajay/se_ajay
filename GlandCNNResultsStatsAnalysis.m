countAllGlands = 1;
%for i=1:size(statsAllFrames,1)
 for i = 49   
    %wnfor j=1:1200
    for j=203
     if isempty(statsAllFrames(i,j).stats) == 1
         continue;
     end
        temp= statsAllFrames(i,j).stats.Solidity;
        tempRnd = statsAllFrames(i,j).stats.Roundness;
        tempWav = statsAllFrames(i,j).stats.Waviness;
        tempEcc = statsAllFrames(i,j).stats.Eccentricity;
        
        tempA = statsAllFrames(i,j).stats.Area;
        tempPer = statsAllFrames(i,j).stats.Perimeter;
        
        for cj=1:length(temp)
            if tempA(cj) < threshArea
                continue;
            end
            stat_SolidityAllGlandsAllCases(countAllGlands) = temp(cj);
            if(temp(cj) < 1 && temp(cj) > 0.9)
                pause1 = 1;
            end
            
            stat_CircularityAllGlandsAllCases(countAllGlands) = min(4*pi*tempA(cj) / ((tempPer(cj) + pi)^2),1);
            stat_WavinessAllGlandsAllCases(countAllGlands) = tempWav(cj);
            stat_EccentricityAllGlandsAllCases(countAllGlands) = tempEcc(cj);
            stat_Area(countAllGlands) = tempA(cj);
            
            countAllGlands = countAllGlands+1;
        end
    end
end