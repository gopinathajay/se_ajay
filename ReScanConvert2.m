function PolarImg = ReScanConvert2(CartImg, res, Rmin, Rmax)

ThetaMin = -pi;
ThetaMax = pi;

[theta, R] = meshgrid(linspace(ThetaMin, ThetaMax, size(PolarImg, 2)), linspace(0, Rmax, size(PolarImg, 1)));
% 
% XI = repmat(linspace(-1, 1, res), res, 1);
% YI = repmat(linspace(-1, 1, res)', 1, res);

thetaI = repmat(linspace(ThetaMin, ThetaMax, ThetaLen), ThetaLen, 1);
rI = repmat(linspace(0, Rmax, Rmax)', 1, Rmax);

% R = sqrt(XI.^2+YI.^2);
% theta = atan2(YI, XI);
Y = rI*sin(thetaI);
X = rI*cos(thetaI);

%PolarImg = interp2(X,Y,double(CartImg),theta,R,'linear');
PolarImg = interp2(theta,R,double(CartImg),X,Y,'linear');

