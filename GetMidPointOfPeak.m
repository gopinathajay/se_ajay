function peakdouble = GetMidPointOfPeak(outVec, maxval, peakdouble)

%Get midpoint of peak
if length(find(outVec == maxval)) > 3;
    ind = find(outVec == maxval);
    peakdouble = ind(round(length(ind)/2));
    
end