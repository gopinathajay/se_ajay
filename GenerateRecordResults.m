function success = GenerateRecordResults(path, script)

command = 'git rev-parse --verify HEAD --short';
[~,gitVer] = system(command);

currPath = pwd;

formatout = 'yyyymmddTHHMMSS'; 
t = datestr(datetime('now'), formatout);

path_new = strcat(path, '\', script, '_', t, '_Git_', gitVer);

mkdir(path_new);

cd(path_new)


success = publish(script, 'outputDir', path_new);
save workspace.mat

cd(currPath);
