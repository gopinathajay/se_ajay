for i=1:1200
vec1 = vecAllFrames(:,i)';    

        
    thresh = mean(vec1) + 2*std(vec1);
    
    pTemp = GetLocalMaxima(vec1', 500, thresh);
    %Get only top 4 hits
    if(length(pTemp)>4)
        p = pTemp(1:4);
    else
        p = [pTemp; zeros(4-length(pTemp),1)];
    end
    listOfPeaks(:,frames) = p;
    
    
    %detect double line

%     if(frames>nFrameTrackStart)
%         
%         TrackDoubleLine
%     end
    
    vec1thresh  = zeros(length(vec1),1)';
    vec1thresh(vec1>thresh) = vec1(vec1>thresh);
%     [r, lags] = xcorr(vec1thresh, DoubleGauss);
%     [val, idx] = max(r);
%     
%     listOfDoubleLine(:,frames) = lags(idx) + length(DoubleGauss)/2;
    se = strel('square', 50);
    [~, listofDoubleLine2(:,i)] = max(1-(vec1thresh) - imopen(1-(vec1thresh), se));
    
end
%     if(frames>nFrameTrackStart)
%         
%         TrackDoubleLine
%     end
    